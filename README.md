# php-extended/php-arrayable-object
A very basic psr-3 compliant logger to log activity in the console.

![coverage](https://gitlab.com/php-extended/php-arrayable-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-arrayable-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-arrayable-object ^8`


## Basic Usage

This library gives an `ArrayableTrait` that implements the `toArray()`
method. Then any object that implements this trait may be called the `toArray()`
method to return an array that represents its internal state.


## License

MIT (See [license file](LICENSE)).
