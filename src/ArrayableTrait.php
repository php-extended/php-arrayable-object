<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-arrayable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Arrayable;

use DateTimeInterface;
use ReflectionClass;
use ReflectionProperty;
use Stringable;

/**
 * ArrayableTrait class file.
 * 
 * This trait allows every object to be converted to a recursive array of
 * primitives and list or maps of primitives.
 * 
 * @author Anastaszor
 */
trait ArrayableTrait
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Arrayable\ArrayableInterface::toArray()
	 * @return array<string, boolean|integer|float|string|array<integer, boolean|integer|float|string>|array<string, boolean|integer|float|string>>
	 */
	public function toArray() : array
	{
		$result = [];
		$rClass = new ReflectionClass($this);
		
		/** @var ReflectionProperty $rProp */
		foreach($rClass->getProperties() as $rProp)
		{
			if($rProp->isStatic())
			{
				continue;
			}
			
			// {{{ php7.4 and php8.0 cannot access private values
			/** @psalm-suppress UnusedMethodCall */
			$rProp->setAccessible(true);
			// }}}
			$value = $rProp->getValue($this);
			
			switch(\gettype($value))
			{
				case 'boolean':
				case 'integer':
				case 'double':
				case 'string':
					$result[$rProp->getName()] = $value;
					break;
				
				case 'array':
					/** @psalm-suppress MixedArgumentTypeCoercion */
					$inner = $this->getArrayFromArray($value);
					if(\count($inner) > 0)
					{
						$result[$rProp->getName()] = $inner;
					}
					break;
					
				case 'object':
					$inner = $this->getArrayFromObject($value);
					if(\is_string($inner))
					{
						$result[$rProp->getName()] = $inner;
						break;
					}
					if(\count($inner) > 0)
					{
						$result[$rProp->getName()] = $inner;
					}
					break;
				
				default:
					// just ignore nulls, and opened or closed resources
			}
		}
		
		return $result;
	}
	
	/**
	 * Gets the array of values from an array of options.
	 * 
	 * @param array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $values
	 * @return array<integer|string, boolean|integer|float|string>
	 * @psalm-suppress InvalidReturnType
	 */
	protected function getArrayFromArray(array $values) : array
	{
		$result = [];
		
		foreach($values as $key => $value)
		{
			switch(\gettype($value))
			{
				case 'boolean':
				case 'integer':
				case 'double':
				case 'string':
					$result[$key] = $value;
					break;
					
				case 'array':
					$inner = $this->getArrayFromArray($value);
					if(\count($inner) > 0)
					{
						$result[$key] = $inner;
					}
					break;
					
				case 'object':
					$inner = $this->getArrayFromObject($value);
					if(\is_string($inner))
					{
						$result[$key] = $inner;
						break;
					}
					if(\count($inner) > 0)
					{
						$result[$key] = $inner;
					}
					break;
					
				default:
					// just ignore nulls, and opened or closed resources
			}
		}
		
		/** @phpstan-ignore-next-line */
		return $result;
	}
	
	/**
	 * Gets the array of values from an object.
	 * 
	 * @param object $object
	 * @return string|array<integer|string, boolean|integer|float|string>
	 * @psalm-suppress MixedInferredReturnType
	 */
	protected function getArrayFromObject(object $object)
	{
		if($object instanceof ArrayableInterface)
		{
			/** @phpstan-ignore-next-line */
			return $object->toArray();
		}
		
		if(\method_exists($object, 'toArray'))
		{
			return $object->toArray();
		}
		
		if($object instanceof Stringable)
		{
			return $object->__toString();
		}
		
		// it seams that even undeclared objects implementing __toString are Stringable
		// @codeCoverageIgnoreStart
		if(\method_exists($object, '__toString'))
		{
			return $object->__toString();
		}
		// @codeCoverageIgnoreEnd
		
		if($object instanceof DateTimeInterface)
		{
			return $object->format(DateTimeInterface::RFC3339_EXTENDED);
		}
		
		/** @psalm-suppress MixedArgumentTypeCoercion */
		return $this->getArrayFromArray(\get_object_vars($object));
	}
	
}
