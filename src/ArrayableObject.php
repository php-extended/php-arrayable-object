<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-arrayable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Arrayable;

/**
 * ArrayableObject class file.
 * 
 * This class only represents the implementation of the ArrayableInterface
 * over the ArrayableTrait.
 * 
 * @author Anastaszor
 */
class ArrayableObject implements ArrayableInterface
{
	use ArrayableTrait;
	
}
