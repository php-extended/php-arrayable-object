<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-arrayable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Arrayable\ArrayableObject;
use PHPUnit\Framework\TestCase;

class ExtendedArrayable extends ArrayableObject
{
	public static int $_notConsidered = 3;
	
	public array $_array = [];
	public ?object $_object = null;
	public ?object $_object2 = null;
	public ?object $_object3 = null;
	public ?object $_object4 = null;
	public ?DateTimeInterface $_date = null;
	
	private bool $_bool = true;
	private int $_int = 1;
	private float $_float = 1.2;
	private string $_string = 'str';
	
}

class StringableObject implements Stringable
{
	public function __toString() : string
	{
		return __CLASS__;
	}
}

class UndeclaredArrayableObject
{
	public function toArray() : array
	{
		return [__CLASS__];
	}
}

class UndeclaredStringableObject
{
	public function __toString() : string
	{
		return __CLASS__;
	}
}

class PrivateObject
{
	public int $_value = 4;
}

/**
 * ArrayableTraitTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Arrayable\ArrayableTrait
 * @internal
 * @small
 */
class ArrayableTraitTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ArrayableObject
	 */
	protected ArrayableObject $_object;
	
	public function testItWorks() : void
	{
		$expected = [
			'_bool' => true,
			'_int' => 1,
			'_float' => 1.2,
			'_string' => 'str',
			'_array' => [
				'bool' => true,
				'int' => 2,
				'float' => 2.2,
				'string' => 'nstr',
				'array' => [
					[
						'value' => 'str',
					],
				],
				'object' => [
					'_bool' => true,
					'_int' => 1,
					'_float' => 1.2,
					'_string' => 'str',
				],
				'otherobject' => 'StringableObject',
			],
			'_object' => [
				'_bool' => true,
				'_int' => 1,
				'_float' => 1.2,
				'_string' => 'str',
			],
			'_object2' => [
				'UndeclaredArrayableObject',
			],
			'_object3' => 'UndeclaredStringableObject',
			'_object4' => [
				'_value' => 4,
			],
			'_date' => '2001-01-01T00:00:00.000+00:00',
		];
		$this->assertEquals($expected, $this->_object->toArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$object = new ExtendedArrayable();
		$object->_array = [
			'bool' => true,
			'int' => 2,
			'float' => 2.2,
			'string' => 'nstr',
			'array' => [
				[
					'value' => 'str',
				],
			],
			'object' => new ExtendedArrayable(),
			'otherobject' => new StringableObject(),
		];
		$object->_object = new ExtendedArrayable();
		$object->_object2 = new UndeclaredArrayableObject();
		$object->_object3 = new UndeclaredStringableObject();
		$object->_object4 = new PrivateObject();
		$object->_date = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01', new DateTimeZone('Europe/London'));
		$this->_object = $object;
	}
	
}
