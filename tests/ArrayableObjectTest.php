<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-arrayable-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Arrayable\ArrayableObject;
use PHPUnit\Framework\TestCase;

/**
 * ArrayableObjectTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Arrayable\ArrayableObject
 * @internal
 * @small
 */
class ArrayableObjectTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ArrayableObject
	 */
	protected ArrayableObject $_object;
	
	public function testItWorks() : void
	{
		$this->assertIsArray($this->_object->toArray());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ArrayableObject();
	}
	
}
